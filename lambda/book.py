import json
def book1(event, context):
    return {
        'statusCode': 200,
        'body': json.dumps("book1")
    }

def book2(event, context):
    return {
        'statusCode': 200,
        'body': json.dumps("book2")
    }

def book3(event, context):
    return {
        'statusCode': 200,
        'body': json.dumps("book3")
    }