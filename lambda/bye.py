import json
import boto3
import os
from datetime import date

def lambda_handler(event, context):
    
    print(event)
    string = event["body"]
    encoded_string = string.encode("utf-8")

    bucket_name = os.environ['BUCKET']
    today = date.today()
    d4 = today.strftime("%b-%d-%Y")
    file_name = d4+".json"
    s3_path = "incoming/" + file_name

    s3 = boto3.resource("s3")
    s3.Bucket(bucket_name).put_object(Key=s3_path, Body=encoded_string)
    return {
        'statusCode': 200,
        'body': json.dumps("file saved")
    }
