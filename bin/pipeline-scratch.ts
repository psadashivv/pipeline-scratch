#!/usr/bin/env node
import * as cdk from 'aws-cdk-lib';
import { WorkshopPipelineStack } from '../lib/pipeline-stack';

const app = new cdk.App();
new WorkshopPipelineStack(app, 'CdkWorkshopPipelineStack', {
  env: {account: '446037945011', region: 'ap-southeast-2'}
});


