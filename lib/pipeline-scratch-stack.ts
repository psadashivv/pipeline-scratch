import * as cdk from 'aws-cdk-lib';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as apigw from 'aws-cdk-lib/aws-apigateway';
import { aws_s3 as s3 } from 'aws-cdk-lib';   
// import { CfnOutput, Construct, Stack, StackProps } from 'constructs';
import * as path from 'path';
import { Construct } from 'constructs';

export class PipelineScratchStack extends cdk.Stack {
  public readonly hcViewerUrl: cdk.CfnOutput;
  public readonly hcEndpoint: cdk.CfnOutput;
  // public readonly urlOutput: CfnOutput;
  
  constructor(scope: Construct, id: string, props?: cdk.StackProps, envName?: string) {
    super(scope, id, props);

    const bucket = new s3.Bucket(this, 'MyEncryptedBucket', {
      encryption: s3.BucketEncryption.KMS,
    });

    // // defines an AWS Lambda resource
    // const hello = new lambda.Function(this, 'HelloHandler', {
    //   runtime: lambda.Runtime.NODEJS_14_X,    // execution environment
    //   code: lambda.Code.fromAsset('lambda'),  // code loaded from "lambda" directory
    //   handler: 'hello.handler'                // file is "hello", function is "handler"
    // });

    // defines an AWS Lambda resource
    const bye = new lambda.Function(this, 'ByeHandler', {
      runtime: lambda.Runtime.PYTHON_3_8,    // execution environment
      code: lambda.Code.fromAsset('lambda'),  // code loaded from "lambda" directory
      handler: 'bye.lambda_handler',           
      environment: {
        'BUCKET': bucket.bucketName,
      },
    });

    // // defines an API Gateway REST API resource backed by our "hello" function.
    // const gateway = new apigw.LambdaRestApi(this, 'projendpoint', {
    //   handler: bye,
    //   proxy: false
    // });

    // const files = gateway.root.addResource('files');
    // files.addMethod('POST'); // POST /files

    // this.hcEndpoint = new cdk.CfnOutput(this, 'GatewayUrl', {
    //   value: gateway.url
    // });

    const book1 = new lambda.Function(this, 'book1', {
      runtime: lambda.Runtime.PYTHON_3_8,    // execution environment
      code: lambda.Code.fromAsset('lambda'),  // code loaded from "lambda" directory
      handler: 'book.book1',           
    });

    const book2 = new lambda.Function(this, 'book2', {
      runtime: lambda.Runtime.PYTHON_3_8,    // execution environment
      code: lambda.Code.fromAsset('lambda'),  // code loaded from "lambda" directory
      handler: 'book.book2',           
    });

    const book3 = new lambda.Function(this, 'book3', {
      runtime: lambda.Runtime.PYTHON_3_8,    // execution environment
      code: lambda.Code.fromAsset('lambda'),  // code loaded from "lambda" directory
      handler: 'book.book3',           
    });

    const whatsname = 'books-api-'+ envName; 
    const api = new apigw.RestApi(this, whatsname);
    
    const books1 = api.root.addResource('book11');
    const getBookIntegration1 = new apigw.LambdaIntegration(book1);
    books1.addMethod('GET', getBookIntegration1);
    const books2 = api.root.addResource('book12');
    const getBookIntegration2 = new apigw.LambdaIntegration(book2);
    books2.addMethod('GET', getBookIntegration2);
    const books3 = api.root.addResource('book13');
    
    const getBookIntegration3 = new apigw.LambdaIntegration(book3);
    books3.addMethod('POST', getBookIntegration3);
  }
}
