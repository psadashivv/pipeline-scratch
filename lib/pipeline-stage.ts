import { PipelineScratchStack } from './pipeline-scratch-stack';
import { Stage, StageProps } from 'aws-cdk-lib';
import { Construct } from 'constructs';

export class WorkshopPipelineStage extends Stage {
    constructor(scope: Construct, id: string, props?: StageProps, envName?: string) {
        super(scope, id, props);

        new PipelineScratchStack(this, 'WebService2', props, envName);
    }
}
