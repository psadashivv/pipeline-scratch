import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as codepipeline from 'aws-cdk-lib/aws-codepipeline';
import {CodeBuildStep, CodePipeline} from "aws-cdk-lib/pipelines";
import {WorkshopPipelineStage} from './pipeline-stage';

export class WorkshopPipelineStack extends cdk.Stack {
    constructor(scope: Construct, id: string, props?: cdk.StackProps) {
        super(scope, id, props);
        
        const sourceOutput = new codepipeline.Artifact();
        const outputSources = new codepipeline.Artifact();
        
        const pipeline = new CodePipeline(this, 'Pipeline', {
          // Encrypt artifacts, required for cross-account deployments
          crossAccountKeys: true,
          pipelineName: 'WorkshopPipeline',
          synth: new CodeBuildStep('SynthStep', {
            input: cdk.pipelines.CodePipelineSource.connection('psadashivv/pipeline-scratch', 'master', {
              connectionArn: 'arn:aws:codestar-connections:ap-southeast-2:446037945011:connection/5c58675b-8266-40e6-8b8f-54d5adaba036', // Created using the AWS console * });',
            }),
            installCommands: [
                      'npm install -g aws-cdk',
                      'npm i @aws-cdk/aws-codepipeline'
                  ],
                  commands: [
                      'npm ci',
                      'npm run build',
                      'npx cdk synth'
                  ]
              }
          )
      });
      
      pipeline.addStage(new WorkshopPipelineStage(this, 'app-dev-sydney', { env: { account: '053019987642', region: 'ap-southeast-2' } }, "dev"));
      // pipeline.addStage(new WorkshopPipelineStage(this, 'app-prod-sydney', { env: { account: '053875622729', region: 'ap-southeast-2' } }));

    }
  }